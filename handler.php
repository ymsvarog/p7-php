<?php
    include 'constants.php';
    include 'uploads.php';

    $Error = 0;
    $AvatarPath = '';

    function place_error()
    {
        global $Error;
        $error_text = '';

        switch($Error)
        {
            case 1:
                $error_text = "Unfilled form";
                break;

            case 2:
                $error_text = "Username exists";
                break;
            
            case 3:
                $error_text = "Email address exists";
                break;

            default:
                $error_text = "Undocumented error";
        }

        echo "<p class=\"materialize-red-text\">$error_text</p>";
    }

    function is_filled_form(): bool
    {
        global $Error;

        if($_POST == NULL){
            $Error = 1;
            return false;
        }
        
        foreach(MANDATORY_FIELDS as $field){
            $value = $_POST[$field];
            filter_input(INPUT_POST, $value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

            if(empty($value)){
                $Error = 1;
                return false;
            }
        }

        return true;
    }

    function is_new_user(): bool
    {
        global $Error;

        if(!file_exists(DB_PATH))
            return true;

        $db_str = file_get_contents(DB_PATH);
        $db_entries = explode("\n", $db_str);

        array_pop($db_entries);

        foreach($db_entries as $entry)
        {
            $fields = explode(",", $entry);

            while(count(MANDATORY_FIELDS) < count($fields   ))
                array_pop($fields);
        
            if( ($fields[0] == $_POST["name"]  && $Error = 2)
              ||($fields[1] == $_POST["email"] && $Error = 3))
                    return false;
        }

        return true;
    }

    function user_to_file()
    {
        global $AvatarPath;

        #if(!file_exists('database/users.csv'))
            #file_put_contents('database/users.csv', '');

        $udb = fopen(DB_PATH, 'a');
        $to_file = "";

        foreach(MANDATORY_FIELDS as $field)
            $to_file .= $_POST[$field] . ",";
        
        $AvatarPath = upload_avatar();

        if(empty($AvatarPath))
            $to_file = substr($to_file, 0, -1);
        else
            $to_file .= $AvatarPath;

        $to_file .= "\n";
        fwrite($udb, $to_file);
        fclose($udb);
    }

    function user_to_page()
    {
        global $AvatarPath;

        echo "<ul>\n";
        foreach(MANDATORY_FIELDS as $field)
            echo "<li>" . ucfirst($field) . ": $_POST[$field]</li>\n";
        echo "</ul>\n";
        
        if(!empty($AvatarPath))
            echo "\t\t\t<img src=\"$AvatarPath\" class=\"avatar\" />\n";
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie-edge" />
    <link rel="stylesheet" href="assets/css/materialize.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <style>
        .container{
            width: 400px;
        }
    </style>
</head>
<body style=" padding-top: 3rem; ">
    <div class="container">
        <?php 
            if(is_filled_form() && is_new_user()){
                user_to_file();
                user_to_page();
            }
            else 
                place_error();
        ?>
        <hr />
        <a class="btn" href="adduser.php">return back</a>
        <a class="btn" href="table.php">view list</a>
    </div>
</body>
</html>