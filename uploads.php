<?php
    function upload_avatar(): string
    {
        $target_dir = AVATARS_PATH . "/" . $_POST["name"];

        if(!mkdir($target_dir))
            return '';
        
        $targetPath = $target_dir . "/" . basename($_FILES["avatar"]["name"]);

        if(!move_uploaded_file($_FILES["avatar"]["tmp_name"], $targetPath))
            return '';  
 
        return $targetPath;  
    }

    function place_avatar($avatar_path)
    {
        echo "<img src=\"$avatar_path\" class=\"avatar\" />";
    }
?>